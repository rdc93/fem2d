import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as mytri
import csv
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

npoints, ntri = np.genfromtxt("elliptic1.dat", dtype=int, max_rows=1, delimiter= ' ')
X1 = np.genfromtxt("elliptic1.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=0, delimiter= ' ')
Y1 = np.genfromtxt("elliptic1.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=1, delimiter= ' ')
Z1 = np.genfromtxt("elliptic1.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=2, delimiter= ' ')
tri1 = np.genfromtxt("elliptic1.dat", dtype=int, max_rows=ntri, skip_header=1+npoints, usecols=(0,1,2), delimiter= ' ')
tri1 = tri1 - 1

npoints, ntri = np.genfromtxt("elliptic3.dat", dtype=int, max_rows=1, delimiter= ' ')
X3 = np.genfromtxt("elliptic3.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=0, delimiter= ' ')
Y3 = np.genfromtxt("elliptic3.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=1, delimiter= ' ')
Z3 = np.genfromtxt("elliptic3.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=2, delimiter= ' ')
tri3 = np.genfromtxt("elliptic3.dat", dtype=int, max_rows=ntri, skip_header=1+npoints, usecols=(0,1,2), delimiter= ' ')
tri3 = tri3 - 1

npoints, ntri = np.genfromtxt("elliptic5.dat", dtype=int, max_rows=1, delimiter= ' ')
X5 = np.genfromtxt("elliptic5.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=0, delimiter= ' ')
Y5 = np.genfromtxt("elliptic5.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=1, delimiter= ' ')
Z5 = np.genfromtxt("elliptic5.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=2, delimiter= ' ')
tri5 = np.genfromtxt("elliptic5.dat", dtype=int, max_rows=ntri, skip_header=1+npoints, usecols=(0,1,2), delimiter= ' ')
tri5 = tri5 - 1

npoints, ntri = np.genfromtxt("elliptic7.dat", dtype=int, max_rows=1, delimiter= ' ')
X7 = np.genfromtxt("elliptic7.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=0, delimiter= ' ')
Y7 = np.genfromtxt("elliptic7.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=1, delimiter= ' ')
Z7 = np.genfromtxt("elliptic7.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=2, delimiter= ' ')
tri7 = np.genfromtxt("elliptic7.dat", dtype=int, max_rows=ntri, skip_header=1+npoints, usecols=(0,1,2), delimiter= ' ')
tri7 = tri7 - 1

fig = plt.figure()
ax1 = fig.add_subplot(221, projection='3d')
tris1 = mytri.Triangulation(X1,Y1,triangles=tri1)
ax1.plot_trisurf(tris1,Z1, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

ax3 = fig.add_subplot(222, projection='3d')
tris3 = mytri.Triangulation(X3,Y3,triangles=tri3)
ax3.plot_trisurf(tris3,Z3, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

ax5 = fig.add_subplot(223, projection='3d')
tris5 = mytri.Triangulation(X5,Y5,triangles=tri5)
ax5.plot_trisurf(tris5,Z5, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

ax7 = fig.add_subplot(224, projection='3d')
tris7 = mytri.Triangulation(X7,Y7,triangles=tri7)
ax7.plot_trisurf(tris7,Z7, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

fig.show()
