#ifndef ELLIPTIC2DHEADERDEF
#define ELLIPTIC2DHEADERDEF

/* Elliptic2D solves a general elliptic PDE
   -del.( a(x,y) del u ) + b(x,y) u = f(x,y)
 using
 the finite element method on any given mesh
 */

#include "FEMInterface.hpp"
#include "Triangle.hpp"
#include <string>
#include <blaze/Math.h>

class Elliptic2D : public FEMInterface
{
public:

	// Function to set the rhs function f and the function on the dirichlet boundary
    void SetFuncs( double(*f)(double, double), double(*a)(double, double), double(*b)(double, double), double (*dir)(double, double) );

	// Override virtual method Compute
	void Compute();

	// Functions to calculate the FEM solution at a point (x,y) and the FEM error across the whole spatial domain
    double FEMSolution( const double x, const double y );
	double FEMError( double(*exact)( double, double ), double p );
    
    // Use the FEMInterface constructor
    Elliptic2D( const std::string input_file, bool (*bound)(double,double) ) : FEMInterface( input_file, bound ){};
    
    void PrintFile( const std::string file_name );

private:

	// Make default constructor private
	Elliptic2D();

	// Declare vector to hold coefficients U;
	blaze::DynamicVector<double> mU;
    
    // Pointer for functions
    double (*mDirFunc)( double, double );
    double (*mFFunc)( double, double );
    double (*mAFunc)( double, double );
    double (*mBFunc)( double, double );
};

#endif
