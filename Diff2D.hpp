#ifndef DIFF2DHEADERDEF
#define DIFF2DHEADERDEF

/* Diff2D solves the diffusion equation:
    u_t = del . ( a del u ) - b u + f
   using the finite element method on a general mesh
 */

#include "FEMInterface.hpp"
#include "Triangle.hpp"
#include <string>
#include <blaze/Math.h>

class Diff2D : public FEMInterface
{
public:
    
    // Function to set the rhs function f and the function on the dirichlet boundary
    void SetFuncs( double(*u0)(double, double), double (*dir)(double, double, double), double(*a)(double,double,double),
                  double(*b)(double,double,double), double (*f)(double,double,double), double timestep, int M );
    
    // Override virtual method Compute
    void Compute( double theta );
    
    // Functions to calculate both the FEM solution and FEM error
    double FEMSolution( const double x, const double y, const double t );
    double FEMError( double(*exact)( double, double, double ), double t, double p );
    void PrintFile( const std::string file_name );
    
    // Use the FEMInterface constructor
    Diff2D( const std::string input_file, bool (*bound)(double,double) ) : FEMInterface( input_file, bound ){};
    
private:
    
    // Make default constructor private
    Diff2D();
    
    // Declare matrix to hold coefficients U, for all time steps;
    blaze::DynamicMatrix<double> mU;
    
    // Pointer for functions
    double (*mDirFunc)( double, double, double );
    double (*mAFunc)( double, double, double );
    double (*mBFunc)( double, double, double );
    double (*mFFunc)( double, double, double );
    double (*mu0Func)( double, double );
    
    int mNumTimeSteps;
    double mTimeStep;
};

#endif
