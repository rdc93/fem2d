#include "FEMInterface.hpp"
#include <iostream>

void FEMInterface::Compute() const
{
}

blaze::DynamicVector<double> FEMInterface::BiCGSTAB(const blaze::CompressedMatrix<double> A, const blaze::DynamicVector<double> b, const int iterations)
{
	int NN(b.size());

	blaze::DynamicVector<double> x(NN, 1.0);
	blaze::DynamicVector<double> r(NN);
	blaze::DynamicVector<double> rhat(NN);
	blaze::DynamicVector<double> p(NN);
	blaze::DynamicVector<double> v(NN);
	blaze::DynamicVector<double> s(NN);
	blaze::DynamicVector<double> t(NN);

	double alpha;
	double w;
	double beta;
	double rho_old;
	double rho_new;

	r = b - A * x;

	rhat = r;

	p = r;

	for (int iteration = 0; iteration < iterations; iteration++)
	{
		v = A * p;

		alpha = (r, rhat) / (v, rhat);

		s = r - alpha * v;

		if ((s, s) < 1e-8)
		{
			x = x + alpha * p;
			break;
		}

		t = A * s;

		w = (t, s) / (t, t);

		x = x + alpha * p + w * s;

		rho_old = (r, rhat);

		r = s - w * t;

		rho_new = (r, rhat);

		if ((r, r) < 1e-8)
		{
			break;
		}

		beta = (alpha / w) * (rho_new / rho_old);

		p = r + beta*(p - w * v);

		if (fabs((r, rhat)) < 1e-8)
		{
			rhat = r;
			p = r;
		}
        
	}
	return x;
}

FEMInterface::FEMInterface( const std::string input_file, bool (*bound)(double,double) )
{
    mBoundFunc = bound;
    
    // Open the input file
    std::ifstream read_file(input_file);
    
    // Import first line as number of nodes and number of elements
    read_file >> mNumNodes >> mNumElements;
    
    blaze::DynamicMatrix<double> ConArray(mNumElements, 3);
    double dummy;
    
    blaze::DynamicVector<double> x(mNumNodes);
    blaze::DynamicVector<double> y(mNumNodes);
    
    // Import x coords and y coords
    for (int i = 0; i < mNumNodes; i++)
    {
        read_file >> x[i] >> y[i] >> dummy;
    }
    
    // Import the connectivity array
    for (int i = 0; i < mNumElements; i++)
    {
        read_file >> ConArray(i, 0) >> ConArray(i, 1) >> ConArray(i, 2);
    }
    
    read_file.close();
    
    mConArray = ConArray;
    mXCoords = x;
    mYCoords = y;
    
    // Get the number of dirichlet and interior nodes
    mNumDirNodes = 0;
    mNumIntNodes = 0;
    
    blaze::DynamicVector<double> DirBoundary(mNumNodes);
    blaze::DynamicVector<double> SecondIndice(mNumNodes);
    
    // Order each interior node and dirichlet node in second indice vector
    for (int i = 0; i < mNumNodes; i++)
    {
        DirBoundary[i] = mBoundFunc(mXCoords[i], mYCoords[i]);
        
        if (DirBoundary[i])
        {
            SecondIndice[i] = mNumDirNodes;
            mNumDirNodes++;
        }
        else
        {
            SecondIndice[i] = mNumIntNodes;
            mNumIntNodes++;
        }
    }
    
    mDirBoundary = DirBoundary;
    mSecondIndice = SecondIndice;
    
    Triangle tri( mConArray, mXCoords, mYCoords );
    
    mTri = tri;
}

double FEMInterface::AvgArea()
{
    double sum=0;
    for (int e=0; e<mNumElements; e++)
    {
        sum += mTri.GetArea(e);
    }
    sum /= mNumElements;
    return sum;
}

double FEMInterface::NumElements()
{
    return mNumElements;
}

double FEMInterface::NumNodes()
{
    return mNumNodes;
}
