#include <iostream>
#include <cmath>
#include "Elliptic2D.hpp"
#include "Diff2D.hpp"
#include "PME2D.hpp"
#include <chrono>

double ellipticRHSFunc( double x, double y );
bool boundaryRect( double x, double y );
bool boundaryCirc( double x, double y );
bool boundaryRectCirc( double x, double y );
double aFunc( double x, double y );
double bFunc( double x, double y );
double exactEllipticFunc( double x, double y );
double exactDiffFunc( double x, double y, double t );
double diffFunc( double x, double y );
double dirDiffFunc( double x, double y, double t );
double diffFFunc( double x, double y, double t );
double diffAFunc( double x, double y, double t );
double diffBFunc( double x, double y, double t );
double simsolPMEFunc( double x, double y, double t );
double simsolPMEFuncu0( double x, double y );

int main(int argc, char* argv[])
{
    std::string mesh;
    std::chrono::duration<double> elapsed;
    
	// pointers to f, b, c functions in elliptic PDE
	double (*f)(double, double);
	f = &ellipticRHSFunc;
    double (*a)(double, double);
    a = &aFunc;
    double (*b)(double, double);
    b = &bFunc;
    
	// pointer to the boundary function for a unit square with corners
    // (0,0) (1,0) (1,1) (0,1)
    bool (*boundrect)(double, double);
	boundrect = &boundaryRect;

	// pointer to elliptic Dirichlet boundary function
	double (*dirEllipt)(double, double);
	dirEllipt = &exactEllipticFunc;
    
    // pointer to exact solution elliptic function
    double (*exactElliptic)(double,double);
    exactElliptic = &exactEllipticFunc;
    
    // Create output file error.dat
    std::ofstream output_file;
    output_file.setf( std::ios::scientific, std::ios::floatfield );
    output_file.precision(6);
    
    output_file.open( "errorsEllipt.dat" );
    assert( output_file.is_open() );
    /*
    auto start = std::chrono::high_resolution_clock::now();
    mesh = "sq1_ele1.mesh";
	Elliptic2D *testElliptic = new Elliptic2D( mesh, boundrect );
    testElliptic->SetFuncs( f, a, b, dirEllipt );
    testElliptic->Compute();
    testElliptic->PrintFile( "elliptic1.dat" );
    // save output to file errorsEllipt.dat
    output_file << std::setw(15) << testElliptic->AvgArea()
                << std::setw(15) << testElliptic->FEMError( exactElliptic, 2.0 )
                << std::endl;
    auto finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Time taken = " << elapsed.count() << "s to solve a "
              << testElliptic->NumNodes() << " node system, with "
              << testElliptic->NumElements() << " elements." << std::endl;
    delete testElliptic;
    
    start = std::chrono::high_resolution_clock::now();
    mesh = "sq1_ele2.mesh";
    testElliptic = new Elliptic2D( mesh, boundrect );
    testElliptic->SetFuncs( f, a, b, dirEllipt );
    testElliptic->Compute();
    // save output to file errorsEllipt.dat
    output_file << std::setw(15) << testElliptic->AvgArea()
                << std::setw(15) << testElliptic->FEMError( exactElliptic, 2.0 )
                << std::endl;
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Time taken = " << elapsed.count() << "s to solve a "
              << testElliptic->NumNodes() << " node system, with "
              << testElliptic->NumElements() << " elements." << std::endl;
    delete testElliptic;
    
    start = std::chrono::high_resolution_clock::now();
    mesh = "sq1_ele3.mesh";
    testElliptic = new Elliptic2D( mesh, boundrect );
    testElliptic->SetFuncs( f, a, b, dirEllipt );
    testElliptic->Compute();
    testElliptic->PrintFile( "elliptic3.dat" );
    // save output to file errorsEllipt.dat
    output_file << std::setw(15) << testElliptic->AvgArea()
                << std::setw(15) << testElliptic->FEMError( exactElliptic, 2.0 )
                << std::endl;
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Time taken = " << elapsed.count() << "s to solve a "
              << testElliptic->NumNodes() << " node system, with "
              << testElliptic->NumElements() << " elements." << std::endl;
    delete testElliptic;
    
    start = std::chrono::high_resolution_clock::now();
    mesh = "sq1_ele4.mesh";
    testElliptic = new Elliptic2D( mesh, boundrect );
    testElliptic->SetFuncs( f, a, b, dirEllipt );
    testElliptic->Compute();
    // save output to file errorsEllipt.dat
    output_file << std::setw(15) << testElliptic->AvgArea()
                << std::setw(15) << testElliptic->FEMError( exactElliptic, 2.0 )
                << std::endl;
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Time taken = " << elapsed.count() << "s to solve a "
              << testElliptic->NumNodes() << " node system, with "
              << testElliptic->NumElements() << " elements." << std::endl;
    delete testElliptic;
    
    start = std::chrono::high_resolution_clock::now();
    mesh = "sq1_ele5.mesh";
    testElliptic = new Elliptic2D( mesh, boundrect );
    testElliptic->SetFuncs( f, a, b, dirEllipt );
    testElliptic->Compute();
    testElliptic->PrintFile( "elliptic5.dat" );
    // save output to file errorsEllipt.dat
    output_file << std::setw(15) << testElliptic->AvgArea()
                << std::setw(15) << testElliptic->FEMError( exactElliptic, 2.0 )
                << std::endl;
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Time taken = " << elapsed.count() << "s to solve a "
              << testElliptic->NumNodes() << " node system, with "
              << testElliptic->NumElements() << " elements." << std::endl;
    delete testElliptic;
    
    start = std::chrono::high_resolution_clock::now();
    mesh = "sq1_ele6.mesh";
    testElliptic = new Elliptic2D( mesh, boundrect );
    testElliptic->SetFuncs( f, a, b, dirEllipt );
    testElliptic->Compute();
    // save output to file errorsEllipt.dat
    output_file << std::setw(15) << testElliptic->AvgArea()
                << std::setw(15) << testElliptic->FEMError( exactElliptic, 2.0 )
                << std::endl;
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Time taken = " << elapsed.count() << "s to solve a "
              << testElliptic->NumNodes() << " node system, with "
              << testElliptic->NumElements() << " elements." << std::endl;
    delete testElliptic;

    start = std::chrono::high_resolution_clock::now();
    mesh = "sq1_ele7.mesh";
    testElliptic = new Elliptic2D( mesh, boundrect );
    testElliptic->SetFuncs( f, a, b, dirEllipt );
    testElliptic->Compute();
    testElliptic->PrintFile( "elliptic7.dat" );
    // save output to file errorsEllipt.dat
    output_file << std::setw(15) << testElliptic->AvgArea()
                << std::setw(15) << testElliptic->FEMError( exactElliptic, 2.0 )
                << std::endl;
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Time taken = " << elapsed.count() << "s to solve a "
              << testElliptic->NumNodes() << " node system, with "
              << testElliptic->NumElements() << " elements." << std::endl;
    delete testElliptic;
    
    output_file.close();
    */
    double (*u0)(double, double);
    u0 = &exactEllipticFunc;
    double (*dirDiff)( double, double, double );
    dirDiff = &exactDiffFunc;
    double (*aDiff)( double, double, double );
    aDiff = &diffAFunc;
    double (*bDiff)( double, double, double );
    bDiff = &diffBFunc;
    double (*fDiff)( double, double, double );
    fDiff = &diffFFunc;
    
    double (*exactDiff)(double,double, double);
    exactDiff = &exactDiffFunc;
    
    std::cout << "Computing Forwards Euler convergence tests..." << std::endl;
    
    output_file.open( "errorsdiffFWD.dat" );
    assert( output_file.is_open() );
    
    mesh = "sq1_ele1.mesh";
    Diff2D *testDiff = new Diff2D( mesh, boundrect );
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, 0.0016, 200 );
    testDiff->Compute( 0 );
    output_file << testDiff->AvgArea() << " " << testDiff->FEMError( exactDiff, 0.32, 1.0 ) << std::endl;
	delete testDiff;
    
    mesh = "sq1_ele2.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, 0.0016, 200 );
    testDiff->Compute( 0 );
    output_file << testDiff->AvgArea() << " " << testDiff->FEMError( exactDiff, 0.32, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele3.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, 0.0016, 200 );
    testDiff->Compute( 0 );
    output_file << testDiff->AvgArea() << " " << testDiff->FEMError( exactDiff, 0.32, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele4.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, 0.0016, 200 );
    testDiff->Compute( 0 );
    output_file << testDiff->AvgArea() << " " << testDiff->FEMError( exactDiff, 0.32, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele5.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, 0.0016, 200 );
    testDiff->Compute( 0 );
    output_file << testDiff->AvgArea() << " " << testDiff->FEMError( exactDiff, 0.32, 1.0 ) << std::endl;
    delete testDiff;
    
    output_file.close();
    
    output_file.open( "errorstimeCN.dat" );
    assert( output_file.is_open() );
    
    std::cout << "Computing time convergence for Crank Nicholson... " << std::endl;
    
    double timestep;
    int numsteps;
    
    mesh = "sq1_ele1.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( .5 );
    testDiff->PrintFile("diff2d1.dat");
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele2.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( .5 );
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele3.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( .5 );
    testDiff->PrintFile("diff2d3.dat");
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele4.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( .5 );
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele5.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( .5 );
    testDiff->PrintFile("diff2d5.dat");
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    output_file.close();
    
    output_file.open( "errorstimeBCK.dat" );
    assert( output_file.is_open() );
    
    std::cout << "Computing time convergence for Backwards Euler... " << std::endl;
    
    mesh = "sq1_ele1.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = sqrt( testDiff->AvgArea() );
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( 1 );
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele2.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( 1 );
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele3.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( 1 );
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele4.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( 1 );
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    mesh = "sq1_ele5.mesh";
    testDiff = new Diff2D( mesh, boundrect );
    timestep = testDiff->AvgArea();
    numsteps = (int) (1/timestep);
    testDiff->SetFuncs( u0, dirDiff, aDiff, bDiff, fDiff, timestep, numsteps );
    testDiff->Compute( 1 );
    output_file << timestep << " " << testDiff->FEMError( exactDiff, 1, 1.0 ) << std::endl;
    delete testDiff;
    
    output_file.close();
    
	// pointer to the boundary
	bool(*boundcirc)(double, double);
	boundcirc = &boundaryCirc;
    
    u0 = &simsolPMEFuncu0;
    
    double(*exactPME)(double, double, double);
    exactPME = &simsolPMEFunc;
    
    mesh = "circ10_ele3.mesh";
	PME2D *testPME = new PME2D(mesh, boundcirc);
	testPME->SetFuncs(u0, exactPME, 1.0, 0.0016, 1250);
	testPME->Compute();
    testPME->PrintFile( "pme2d.dat" );
    std::cout << testPME->FEMError( exactPME, 2, 1.0);
	delete testPME;
    
	return 0;
}

double ellipticRHSFunc(double x, double y)
{
    double output;
    output = M_PI * y * sin( M_PI * x ) * cos( M_PI * y )
           + M_PI * x * cos( M_PI * x ) * sin( M_PI * y )
           + 2 * M_PI * M_PI * x * y * cos( M_PI * x ) * cos( M_PI * y );
    return output;
}

double aFunc( double x, double y )
{
    return x * y;
}

double bFunc(double x, double y)
{
    return 0;
}

bool boundaryRect(double x, double y)
{
    bool output=false;

    if (fabs(x) < 1e-9)
    {
        output = true;
    }
    else if (fabs(x - 1.0) < 1e-9)
    {
        output = true;
    }
    else if (fabs(y) < 1e-9)
    {
        output = true;
    }
    else if (fabs(y - 1.0) < 1e-9)
    {
        output = true;
    }

    return output;
}

bool boundaryCirc( double x, double y )
{
    bool output=false;
    
    double r = sqrt( x*x + y*y );
    
    if ( fabs( r - 10.0 ) <  1e-8 )
    {
        output = true;
    }
    
    return output;
}

bool boundaryRectCirc( double x, double y )
{
    bool output=false;
    
    if ( x < 0 )
    {
        if( fabs( x*x + pow( (y - 0.5), 2 ) - 0.25 ) < 1e-8 )
        {
            output = true;
        }
    } else if ( x > 1.0 )
    {
        if( fabs( pow( (x - 1.0), 2 ) + pow( (y - 0.5), 2 ) - 0.25 ) < 1e-8 )
        {
            output = true;
        }
    } else if ( fabs( y - 1.0 ) < 1e-8 )
    {
        output = true;
    } else if ( fabs( y ) < 1e-8 )
    {
        output = true;
    }
    
    return output;
}

double exactEllipticFunc(double x, double y)
{
    double output=0;
    output = cos( M_PI * x ) * cos( M_PI * y );
    return output;
}

double dirDiffFunc( double x, double y, double t )
{
    double output=0;
    
    return output;
}

double diffFFunc( double x, double y, double t )
{
    double output = 0;
    output = exp(-t) * cos( M_PI * x ) * cos( M_PI * y ) * ( 2 * M_PI * M_PI - 1 );
    return output;
}

double diffAFunc( double x, double y, double t )
{
    return 1.0;
}

double diffBFunc( double x, double y, double t )
{
    return 0;
}

double exactDiffFunc( double x, double y, double t )
{
    double output=0;
    
    output = exp(-t) * cos( M_PI * x ) * cos( M_PI * y ) ;
    
    return output;
}

double simsolPMEFunc( double x, double y, double t )
{
    double output=0;
    
    double m=1;
    
    double r0=1;
    
    double r = sqrt(x*x + y*y);
    
    double t0 = r0*r0*m / (2.0 * ( 2.0 + 2.0 * m ) );

    double tcurrent = t0 + t;
    
    double lambda = pow( (tcurrent / t0), 1.0 / ( 2.0 + 2.0 * m ) );
    
    if( r < r0 * lambda )
    {
        output = ( 1.0 / (lambda*lambda) ) * pow( 1.0 - pow( ( r / ( r0 * lambda ) ), 2.0 ), 1.0 / m );
    }
    
    return output;
}

double simsolPMEFuncu0( double x, double y )
{
    double output=0;
    
    double r = x*x + y*y;
    
    if( r < 1 )
    {
        output = 1.0 - r;
    }
    
    return output;
}
