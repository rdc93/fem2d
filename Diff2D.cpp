#include "Diff2D.hpp"
#include <cmath>
#include <iostream>
#include <blaze/Math.h>

void Diff2D::SetFuncs( double(*u0)(double, double), double (*dir)(double, double, double), double(*a)(double,double,double),
                       double(*b)(double, double, double), double (*f)(double, double, double), double timestep, int M )
{
    // Set functions
    mAFunc = a;
    mBFunc = b;
    mFFunc = f;
    
    mNumTimeSteps = M;
    mTimeStep = timestep;
    
    mDirFunc = dir;
    mu0Func = u0;
    
    blaze::DynamicMatrix<double> u(mNumNodes, mNumTimeSteps+1, 0);
    mU = u;
    
    // Set coefficients corresponding to dirichlet conditions
    for (int j = 0; j < mNumNodes; j++)
    {
        if (mDirBoundary[j])
        {
            for (int m=0; m<mNumTimeSteps+1; m++)
            {
                mU(j,m) = mDirFunc(mXCoords[j], mYCoords[j], m*mTimeStep);
            }
        }
    }
}

void Diff2D::Compute( double theta )
{
    // u vector
    blaze::DynamicVector<double> u(mNumIntNodes, 0);
    
    // mass matrix
    blaze::CompressedMatrix<double> mass(mNumIntNodes, mNumIntNodes);
    
    // Vectors for Gauss points and coefficients
	blaze::DynamicVector<double> GaussCoords;
	blaze::DynamicVector<double> GaussCoeffs = mTri.GetQuintGaussCoeffs();

    // Iterate over all elements
    for (int e = 0; e<mNumElements; e++)
    {
		GaussCoords = mTri.GetQuintGaussCoords(e);

		// iterate over each local node
        for (int J=0; J<3; J++)
        {
            // Gives the global number of node which is Jth vertex of element e
            int j = mConArray(e, J) - 1;
            int indice_j = mSecondIndice[mConArray(e, J) - 1];
            
            if (not(mDirBoundary[j]))
            {
                for (int k=0; k<7; k++)
                {   
                    // Calculate the integral of u0 * phi_j, 
                    u[indice_j] += GaussCoeffs[k] * mu0Func( GaussCoords[2*k], GaussCoords[2*k+1] )
					       * mTri.GetArea(e) * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J );
                }
                
                for (int I=0; I<3; I++)
                {
                    // Gives the global number of node which is the Ith vertex of element e
                    int i = mConArray(e, I) - 1;
                    int indice_i = mSecondIndice[mConArray(e, I) - 1];
                    
                    if ( not(mDirBoundary[i]) )
                    {  
                        // From Jimack document: mass_entry += Area / 6 if I = J
                        //                                  += Area / 12 if I != J
                        if ( I == J )
                        {
                            mass( indice_j, indice_i ) += mTri.GetArea(e) / 6.0;
                        } else
                        {
                            mass( indice_j, indice_i ) += mTri.GetArea(e) / 12.0;
                        }
                    }
                }
            }
        }
    }

    blaze::DynamicMatrix<double> LHSmat = mass;
    
    // Solve the system MU = U0 for U
    if ( isSymmetric(LHSmat) )
    {
        // If the matrix is symmetric then call cholesky based solver
        posv( LHSmat, u, 'U');
    } else 
    {
        // if not symmetric then call LU based solver
        const std::unique_ptr<int[]> ipiv( new int[mNumIntNodes] );
        gesv( LHSmat, u, ipiv.get() );
    }
    
    // Place u into mU matrix
    int i=0;
    for (int j=0; j<mNumNodes; j++)
    {
        if (not(mDirBoundary[j]))
        {
            mU(j, 0) = u[i];
            i++;
        }
    }
    
    // Iterate over all time steps
    for (int m=1; m<mNumTimeSteps+1; m++)
    {
        // Create placeholders for the right hand matrices, with M being the mass-style matrix corresponding to
        // the integral of ( c(x,y,t) phi_i phi_j ), and K the stiffness-style matrix corresponding to the
        // integral of ( D(x,y,t) del(phi_i) . del(phi_j) )
        blaze::CompressedMatrix<double> K_RHS( mNumIntNodes, mNumIntNodes );
        blaze::DynamicMatrix<double> K_LHS( mNumIntNodes, mNumIntNodes, 0 );
        // Create placeholder for the F vector, the integral of ( f(x,y,t) phi_j )
        blaze::DynamicVector<double> F( mNumIntNodes , 0 );
        
        double oldtime = (m-1) * mTimeStep;
        double newtime =     m * mTimeStep;

		// Iterate over all elements
        for (int e=0; e<mNumElements; e++)
        {
			GaussCoords = mTri.GetQuintGaussCoords(e);

			// Integrate over three nodes 0, 1, 2
            for (int J=0; J<3; J++)
            {
                // Gives the global number of node which is the Jth local node of element e
                int j = mConArray( e, J ) - 1;
                int indice_j = mSecondIndice[mConArray(e, J) - 1];
                
                if (not(mDirBoundary[j]))
                {
                    for (int k=0; k<7; k++)
                    {
                        // Calculate the integral of u0 * phi_j, 
                        F[indice_j] += GaussCoeffs[k] * ( ( 1.0 - theta ) * mFFunc( GaussCoords[2*k], GaussCoords[2*k+1], oldtime )
                                                                + theta   * mFFunc( GaussCoords[2*k], GaussCoords[2*k+1], newtime ) )
                                      * mTimeStep * mTri.GetArea(e) * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J );
                    }

                    for (int I=0; I<3; I++)
                    {
                        // Gives the number of node which is the Ith local node of element e
                        int i = mConArray(e, I) - 1;
                        int indice_i = mSecondIndice[mConArray(e, I) - 1];

                        // delta = del(phi_i) . del(phi_j), used for stiffness style matrix
                        double delta  = mTri.dxPhiFunc(e, I) * mTri.dxPhiFunc(e, J);
                        delta += mTri.dyPhiFunc(e, I) * mTri.dyPhiFunc(e, J);

                        // stiff_old/stiff_new is the stiffness style matrix, the integral of a(x,y,t) del phi_i . del phi_j
                        double stiff_old = 0;
                        double stiff_new = 0;
                        // mass_old/mass_new is the mass style matrix, the integral of b(x,y,t) phi_i phi_j
                        double mass_old = 0;
                        double mass_new = 0;
                        // mass is the mass matrix entry
                        double mass_entry = 0;

                        for (int k=0; k<7; k++)
                        {
                            // Stiffness-style matrix
                            stiff_old += GaussCoeffs[k] * mAFunc( GaussCoords[2*k], GaussCoords[2*k+1], oldtime )
                                                        * mTri.GetArea(e) * delta;
                            stiff_new += GaussCoeffs[k] * mAFunc( GaussCoords[2*k], GaussCoords[2*k+1], newtime )
                                                        * mTri.GetArea(e) * delta;
                            
                            // mass-style matrix
                            mass_old += GaussCoeffs[k] * mBFunc( GaussCoords[2*k], GaussCoords[2*k+1], oldtime )
                                                 * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J )
                                                 * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, I )
                                                 * mTri.GetArea(e);
                            mass_new += GaussCoeffs[k] * mBFunc( GaussCoords[2*k], GaussCoords[2*k+1], newtime )
                                                 * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J )
                                                 * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, I )
                                                 * mTri.GetArea(e);
                            // actual mass
                            mass_entry += GaussCoeffs[k] * mTri.GetArea(e)
                                        * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J )
                                        * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, I );
                        }
                    
                        if ( mDirBoundary[i] )
                        {
                            F[indice_j] += mU(i, m-1) * (   mass_entry - mTimeStep * ( 1.0 - theta ) * ( stiff_old + mass_old ) )
                                         + mU(i, m  ) * ( - mass_entry - mTimeStep *         theta   * ( stiff_new + mass_new ) );
                        } else
                        {
                            K_RHS( indice_j, indice_i ) += mass_entry - ( 1.0 - theta ) * mTimeStep * ( stiff_old + mass_old );
                            K_LHS( indice_j, indice_i ) += mass_entry         + theta   * mTimeStep * ( stiff_new + mass_new );
                        }
                    }
                }
            }
        }

        // Update u
        u = K_RHS * u + F;

        // Call the linear solver of choice
        if ( isSymmetric( K_LHS ) )
        {
            // If the matrix is symmetric then call cholesky based solver
            posv( K_LHS, u, 'U');
        } else 
        {
            // if not symmetric then call LU based solver
            const std::unique_ptr<int[]> ipiv( new int[mNumIntNodes] );
            gesv( K_LHS, u, ipiv.get() );
        }

        // Place u at current time step into mU matrix
        int i=0;
        for (int j=0; j<mNumNodes; j++)
        {
            if (not(mDirBoundary[j]))
            {
                mU(j, m) = u[i];
                i++;
            }
        }
    }
}

double Diff2D::FEMSolution( const double x, const double y, const double t )
{
    double sum=0;
    
    int indice_time = (int)(t/mTimeStep);
    
    // Iterate over all elements e
    for (int e = 0; e<mNumElements; e++)
    {
        // First check if the point is in a triangle
        if ( mTri.IsInTri( x, y, e ) )
        {
			// Then iterate over all local nodes
            for (int J=0; J<3; J++)
            {
				// Get global number of current node
                int indice_j = mConArray(e, J) - 1;
                
                // Then add to the sum the output of each basis function
                sum += mU( indice_j, indice_time ) * mTri.phiFunc(x, y, e, J);
            }
            
            // Break if already appeared in one triangle
            break;
        }
    }
    
    return sum;
}

double Diff2D::FEMError( double(*exact)( double, double, double ), double t, double p)
{
    double sum = 0;
    int indice_time = (int)(t/mTimeStep);

    // Create vectors to hold gauss quadrature information
    blaze::DynamicVector<double> GaussCoords;
    blaze::DynamicVector<double> GaussCoeffs = mTri.GetQuintGaussCoeffs();
    
    for (int e=0; e<mNumElements; e++)
    {
        // Get the coordinate data for current element
        GaussCoords = mTri.GetQuintGaussCoords(e);
        
        for (int k=0; k<7; k++)
        {
            // Find the FEM solution on current element
            double femsol=0;
            for (int I=0; I<3; I++)
            {
                int indice_i = mConArray( e, I ) - 1;
                femsol += mU(indice_i,indice_time) * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, I );
            }

            // Find the error
            sum += GaussCoeffs[k] * mTri.GetArea(e) 
                * pow ( fabs ( exact( GaussCoords[2*k], GaussCoords[2*k+1], t ) - femsol ) , p );
        }
    }
    
    return pow(sum, 1 / p);
}

void Diff2D::PrintFile( const std::string file_name )
{
    std::ofstream print_file;
    print_file.open( file_name );
    
    print_file << mNumNodes << " " << mNumElements << std::endl;
    for (int i=0; i<mNumNodes; i++)
    {
        print_file << mXCoords[i] << " " << mYCoords[i] << " ";
        for (int t=0; t<=mNumTimeSteps; t++)
        {
            print_file << mU(i, t) << " ";
        }
        print_file << std::endl;
    }
    for (int i=0; i<mNumElements; i++)
    {
        print_file << mConArray(i, 0) << " "
        << mConArray(i, 1) << " "
        << mConArray(i, 2) << " " << std::endl;
    }
    
    print_file.close();
}
