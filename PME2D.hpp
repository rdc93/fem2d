#ifndef PME2DHEADERDEF
#define PME2DHEADERDEF

/* PME2D solves the diffusion equation:
 u_t = del ( u^m * del u )
 using the finite element method with forward euler time step
 on a general mesh
 */

#include "FEMInterface.hpp"
#include "Triangle.hpp"
#include <string>
#include <blaze/Math.h>

class PME2D : public FEMInterface
{
public:
    
    // Function to set the rhs function f and the function on the dirichlet boundary
    void SetFuncs( double(*u0)(double, double), double (*dir)(double, double, double), double m, double timestep, int M );
    
    // Override virtual method Compute
    void Compute( );
    
    // Functions to calculate both the FEM solution and FEM error
    double FEMSolution( const double x, const double y, const double t );
    double FEMError( double(*exact)( double, double, double ), double t, double p );
    
    // Use the FEMInterface constructor
    PME2D( const std::string input_file, bool (*bound)(double,double) ) : FEMInterface( input_file, bound ){};
    
    void PrintFile( const std::string file_name );
    
private:
    
    // Make default constructor private
    PME2D();
    
    // Declare matrix to hold coefficients U, for all time steps;
    blaze::DynamicMatrix<double> mU;
    
    // Pointer for functions
    double (*mDirFunc)( double, double, double );
    double (*mu0Func)( double, double );
    
    double mPMEConst;
    int mNumTimeSteps;
    double mTimeStep;
};

#endif
