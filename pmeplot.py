import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as mytri
import csv
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

npoints, ntri = np.genfromtxt("PME2D.dat", dtype=int, max_rows=1, delimiter= ' ')
X = np.genfromtxt("PME2D.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=0, delimiter= ' ')
Y = np.genfromtxt("PME2D.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=1, delimiter= ' ')
Z1 = np.genfromtxt("PME2D.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=2, delimiter= ' ')
Z2 = np.genfromtxt("PME2D.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=500, delimiter= ' ')
tri = np.genfromtxt("PME2D.dat", dtype=int, max_rows=ntri, skip_header=1+npoints, usecols=(0,1,2), delimiter= ' ')
tri = tri - 1

fig = plt.figure()
ax1 = fig.add_subplot(121, projection='3d')
tris = mytri.Triangulation(X,Y,triangles=tri)
ax1.plot_trisurf(tris,Z1, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

ax2 = fig.add_subplot(122, projection='3d')
ax2.plot_trisurf(tris,Z2, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

fig.show()
