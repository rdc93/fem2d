import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as mytri
import csv
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

npoints, ntri = np.genfromtxt("diff2d3.dat", dtype=int, max_rows=1, delimiter= ' ')
X1 = np.genfromtxt("diff2d3.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=0, delimiter= ' ')
Y1 = np.genfromtxt("diff2d3.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=1, delimiter= ' ')
Z1 = np.genfromtxt("diff2d3.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=2, delimiter= ' ')
Z11 = np.genfromtxt("diff2d3.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=164, delimiter= ' ')
tri1 = np.genfromtxt("diff2d3.dat", dtype=int, max_rows=ntri, skip_header=1+npoints, usecols=(0,1,2), delimiter= ' ')
tri1 = tri1 - 1
tris1 = mytri.Triangulation(X1,Y1,triangles=tri1)

npoints, ntri = np.genfromtxt("diff2d5.dat", dtype=int, max_rows=1, delimiter= ' ')
X2 = np.genfromtxt("diff2d5.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=0, delimiter= ' ')
Y2 = np.genfromtxt("diff2d5.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=1, delimiter= ' ')
Z2 = np.genfromtxt("diff2d5.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=2, delimiter= ' ')
Z22 = np.genfromtxt("diff2d5.dat", dtype=float, max_rows = npoints, skip_header=1, usecols=1998, delimiter= ' ')
tri2 = np.genfromtxt("diff2d5.dat", dtype=int, max_rows=ntri, skip_header=1+npoints, usecols=(0,1,2), delimiter= ' ')
tri2 = tri2 - 1
tris2 = mytri.Triangulation(X2,Y2,triangles=tri2)

fig = plt.figure()
ax1 = fig.add_subplot(221, projection='3d')
ax1.plot_trisurf(tris1,Z1, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

ax2 = fig.add_subplot(222, projection='3d')
ax2.plot_trisurf(tris1,Z11, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

ax3 = fig.add_subplot(223, projection='3d')
ax3.plot_trisurf(tris2,Z2, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")

ax4 = fig.add_subplot(224, projection='3d')
ax4.plot_trisurf(tris2,Z22, color=(0,0,0,0), linewidth=0.3, edgecolor="Black")


fig.show()
