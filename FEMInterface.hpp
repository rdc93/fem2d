#ifndef FEMINTERFACEHEADERDEF
#define FEMINTERFACEHEADERDEF

#include <string>
#include <cmath>
#include <fstream>
#include <blaze/Math.h>
#include "Triangle.hpp"

// Interface class for 2D PDE problems for finite element methods to use

class FEMInterface
{
public:
	// Computes the coefficients U given a function f
	virtual void Compute() const;

	// iterative solver - using BiCGSTAB, A = sparse matrix, x is initial guess vector, b is RHS vector
	// Solves the system Ax = b for x.
	blaze::DynamicVector<double> BiCGSTAB(const blaze::CompressedMatrix<double> A, const blaze::DynamicVector<double> b, const int iterations);
    
    // Imports coordinates of nodes and
    FEMInterface( const std::string input_file, bool (*bound)(double,double) );
    
    // Gets average area over all elements in the triangulation
    double AvgArea();
    
    double NumElements();
    
    double NumNodes();
    
protected:
    // Some members that appear in all derived PDE FEMs
    int mNumNodes;
    int mNumElements;
    int mNumDirNodes;
    int mNumIntNodes;
    
    // matrix to hold the connectivity array
    blaze::DynamicMatrix<int> mConArray;
    
    // vectors to hold the node coordinates
    blaze::DynamicVector<double> mXCoords;
    blaze::DynamicVector<double> mYCoords;
    blaze::DynamicVector<bool> mDirBoundary;
    blaze::DynamicVector<int> mSecondIndice;
    
    // pointer for the boundary function
    bool (*mBoundFunc)(double, double);
    
    Triangle mTri;
};

#endif
