#include "Elliptic2D.hpp"
#include <cmath>
#include <iostream>
#include <blaze/Math.h>
#include <fstream>

void Elliptic2D::SetFuncs( double(*f)(double, double), double(*a)(double, double), double(*b)(double, double), double (*dir)(double, double) )
{
    blaze::DynamicVector<double> u(mNumNodes, 0);
    mU = u;
    
    mDirFunc = dir;
    mFFunc = f;
    mAFunc = a;
    mBFunc = b;
    
    // Set coefficients corresponding to dirichlet conditions
    for (int j = 0; j < mNumNodes; j++)
    {
        if (mDirBoundary[j])
        {
            mU[j] = mDirFunc(mXCoords[j], mYCoords[j]);
        }
    }
}

void Elliptic2D::Compute()
{
	// RHS vector
	blaze::DynamicVector<double> F(mNumIntNodes, 0);
    
    // K matrix
	blaze::DynamicMatrix<double> K( mNumIntNodes, mNumIntNodes, 0 );

    blaze::DynamicVector<double> GaussCoords;
    blaze::DynamicVector<double> GaussCoeffs = mTri.GetQuintGaussCoeffs();
	
	// Iterate over all elements e
	for (int e = 0; e<mNumElements; e++)
	{
        GaussCoords = mTri.GetQuintGaussCoords(e);
        
		for (int J = 0; J < 3; J++)
		{
			// Gives the number of node which is Jth vertex of element e
            int j = mConArray(e, J) - 1;
			int indice_j = mSecondIndice[mConArray(e, J) - 1];

			if (not(mDirBoundary[j]))
			{
                for (int k=0; k<7; k++)
                {
                    F[indice_j] += GaussCoeffs[k] * mFFunc( GaussCoords[2*k], GaussCoords[2*k+1] )
                                * mTri.GetArea(e) * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J );
                }

				for (int I = 0; I < 3; I++)
				{
					// Gives the number of node which is the Ith vertex of element e
                    int i = mConArray(e, I) - 1;
                    int indice_i = mSecondIndice[mConArray(e, I) - 1];

					// integral of del phi_i . del phi_j on an element is
					// Area * ( del phi_i . del phi_j )
					// Since each del phi_i is constant for all (x,y) in each element
					// del phi_i . del phi_j = dphi_i/dx * dphi_j/dx + dphi_i/dy * dphi_j/dy
					double delta  = mTri.dxPhiFunc(e, I) * mTri.dxPhiFunc(e, J);
                           delta += mTri.dyPhiFunc(e, I) * mTri.dyPhiFunc(e, J);
                    
                    // placeholder so as not to calculate the same entry twice for the RHS vector
                    // and the stiffness/mass matrices
                    double stiff_entry=0;
                    double mass_entry=0;
                    
                    for (int k=0; k<7; k++)
                    {
                        stiff_entry += GaussCoeffs[k] * mAFunc( GaussCoords[2*k], GaussCoords[2*k+1] )
                                    * mTri.GetArea(e) * delta;
                        mass_entry += GaussCoeffs[k] * mBFunc( GaussCoords[2*k], GaussCoords[2*k+1] )
                        * mTri.GetArea(e) * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, I )
                                          * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J );
                    }
                    
					if (mDirBoundary[i])
					{
						F[indice_j] -= mU[i] * ( stiff_entry + mass_entry );
					}
					else
					{
						K( indice_j, indice_i ) += stiff_entry + mass_entry;
					}
				}
			}
		}
	}
    
    if ( isSymmetric(K) )
    {
        // If the matrix is symmetric then call cholesky based solver
        posv( K, F, 'U');
    } else 
    {
        // if not symmetric then call LU based solver
        const std::unique_ptr<int[]> ipiv( new int[mNumIntNodes] );
        gesv( K, F, ipiv.get() );
    }
    
    // Place F into the mU vector
    int i=0;
    for (int j=0; j<mNumNodes; j++)
    {
        if (not(mDirBoundary[j]))
        {
            mU[j] = F[i];
            i++;
        }
    }
}

double Elliptic2D::FEMSolution(const double x, const double y)
{
	double sum=0;
    
    // Iterate over all elements e
    for (int e = 0; e<mNumElements; e++)
    {
        // First check if the point is in a triangle
        if ( mTri.IsInTri( x, y, e ) )
        {
            for (int J=0; J<3; J++)
            {
                int indice_j = mConArray(e, J) - 1;
                
                // Then add to the sum the output of each basis function
                sum += mU[indice_j] * mTri.phiFunc(x, y, e, J);
            }
            
            // Break if already appeared in one triangle
            break;
        }
    }

	return sum;
}

double Elliptic2D::FEMError(double(*exact)(double,double), double p)
{
	double sum = 0;

    // Create vectors to hold gauss quadrature information
    blaze::DynamicVector<double> GaussCoords;
    blaze::DynamicVector<double> GaussCoeffs = mTri.GetQuintGaussCoeffs();
    
    for (int e=0; e<mNumElements; e++)
    {
        // Get the coordinate data for current element
        GaussCoords = mTri.GetQuintGaussCoords(e);
        
        for (int k=0; k<7; k++)
        {
            double femsol=0;
            for (int I=0; I<3; I++)
            {
                int indice_i = mConArray( e, I ) - 1;
                femsol += mU[indice_i] * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, I );
            }
            
            sum += GaussCoeffs[k] * mTri.GetArea(e)
                * pow ( fabs ( exact( GaussCoords[2*k], GaussCoords[2*k+1] ) - femsol ) , p );
        }
    }
    
	return pow(sum, 1 / p);
}

void Elliptic2D::PrintFile( const std::string file_name )
{
    std::ofstream print_file;
    print_file.open( file_name );
    
    print_file << mNumNodes << " " << mNumElements << std::endl;
    for (int i=0; i<mNumNodes; i++)
    {
        print_file << mXCoords[i] << " " << mYCoords[i] << " " << mU[i] << std::endl;
    }
    for (int i=0; i<mNumElements; i++)
    {
        print_file << mConArray(i, 0) << " " 
                   << mConArray(i, 1) << " "
                   << mConArray(i, 2) << " " << std::endl;
    }
    
    print_file.close();
}
