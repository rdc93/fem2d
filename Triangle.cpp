#include "Triangle.hpp"
#include <cmath>
#include <iostream>

Triangle::Triangle( const blaze::DynamicMatrix<int> ConArray, const blaze::DynamicVector<double> x,
                    const blaze::DynamicVector<double> y )
{
    int numElements = (int)(ConArray.rows());
    
    blaze::DynamicMatrix<double> A(numElements,3);
    blaze::DynamicMatrix<double> B(numElements,3);
    blaze::DynamicMatrix<double> C(numElements,3);
    
    blaze::DynamicVector<double> area(numElements);
    
    // Constants to create gauss matrix
    double alpha1 = 0.0597158717;
    double alpha2 = 0.7974269853;
    double beta1 = 0.4701420641;
    double beta2 = 0.1012865073;
    
    // Matrix to hold all gauss points
    blaze::DynamicMatrix<double> gaussPoints(14, numElements);

    for (int e=0; e<numElements; e++)
    {
        double x1 = x[ConArray(e, 0) - 1];
        double x2 = x[ConArray(e, 1) - 1];
        double x3 = x[ConArray(e, 2) - 1];
        double y1 = y[ConArray(e, 0) - 1];
        double y2 = y[ConArray(e, 1) - 1];
        double y3 = y[ConArray(e, 2) - 1];
        
        A(e, 0) = x2 * y3 - x3 * y2;
        A(e, 1) = x3 * y1 - x1 * y3;
        A(e, 2) = x1 * y2 - x2 * y1;
        
        B(e, 0) = y2 - y3;
        B(e, 1) = y3 - y1;
        B(e, 2) = y1 - y2;
        
        C(e, 0) = x3 - x2;
        C(e, 1) = x1 - x3;
        C(e, 2) = x2 - x1;
        
        area[e] = 0.5 * ( C(e, 2) * B(e, 1) - C(e, 1) * B(e, 2) );
        
        gaussPoints(0, e) = ( x1 + x2 + x3 ) / 3.0;
        gaussPoints(1, e) = ( y1 + y2 + y3 ) / 3.0;
        
        gaussPoints(2, e) = alpha1 * x1 + beta1 * x2 + beta1 * x3;
        gaussPoints(3, e) = alpha1 * y1 + beta1 * y2 + beta1 * y3;
        
        gaussPoints(4, e) = beta1 * x1 + alpha1 * x2 + beta1 * x3;
        gaussPoints(5, e) = beta1 * y1 + alpha1 * y2 + beta1 * y3;
        
        gaussPoints(6, e) = beta1 * x1 + beta1 * x2 + alpha1 * x3;
        gaussPoints(7, e) = beta1 * y1 + beta1 * y2 + alpha1 * y3;
        
        gaussPoints(8, e) = alpha2 * x1 + beta2 * x2 + beta2 * x3;
        gaussPoints(9, e) = alpha2 * y1 + beta2 * y2 + beta2 * y3;
        
        gaussPoints(10, e) = beta2 * x1 + alpha2 * x2 + beta2 * x3;
        gaussPoints(11, e) = beta2 * y1 + alpha2 * y2 + beta2 * y3;
        
        gaussPoints(12, e) = beta2 * x1 + beta2 * x2 + alpha2 * x3;
        gaussPoints(13, e) = beta2 * y1 + beta2 * y2 + alpha2 * y3;
    }
    
	mA = A;
	mB = B;
	mC = C;

    mArea = area;
    
    mGaussPoints = gaussPoints;
}

Triangle::Triangle()
{
}

double Triangle::phiFunc( const double x, const double y, const int e, const int j )
{
    return ( mA(e, j) + mB(e, j) * x + mC(e, j) * y ) / ( 2.0 * mArea[e] );
}

bool Triangle::IsInTri(const double x, const double y, const int e )
{
	bool output = false;

	// Determine if the point (x, y) is in element e
	// In element if s >= 0, t >= 0 and s + t <= 2*area
	double s;
	double t;
	s = mA(e, 1) + x * mB(e, 1) + y * mC(e, 1);
	t = mA(e, 2) + x * mB(e, 2) + y * mC(e, 2);

	if ((s >= 0) && (t >= 0) && (s + t <= 2 * mArea[e]))
	{
		output = true;
	}

	return output;
}

double Triangle::dxPhiFunc( const int e, const int j )
{
	return mB(e, j) / ( 2.0*mArea[e] );
}

double Triangle::dyPhiFunc( const int e, const int j )
{
	return mC(e, j) / ( 2.0*mArea[e] );
}

double Triangle::GetArea( const int e )
{
	return mArea[e];
}

blaze::DynamicVector<double> Triangle::GetQuintGaussCoords( const int e )
{
    blaze::DynamicVector<double> A(14);
    for (int i=0; i<14; i++)
    {
        A[i] = mGaussPoints(i, e);
    }
    return A;
}

blaze::DynamicVector<double> Triangle::GetQuintGaussCoeffs()
{
    blaze::DynamicVector<double> coeffs(7, 0);
    
    coeffs[0] = 0.225;
    coeffs[1] = 0.1323941527;
    coeffs[2] = 0.1323941527;
    coeffs[3] = 0.1323941527;
    coeffs[4] = 0.1259391805;
    coeffs[5] = 0.1259391805;
    coeffs[6] = 0.1259391805;
    
    return coeffs;
}
