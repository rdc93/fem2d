import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

mpl.rcParams['legend.fontsize'] = 10

plt.figure(1)
h,err = np.loadtxt("errorsEllipt.dat", unpack=True)
hVals = np.linspace(1E-1, 1E-5, 1000)
eVals = hVals;

ax2 = plt.subplot(111)
ax2.loglog(h, err, '.', hVals,eVals,'-')
plt.legend(['Numerical error','slope = 1'])
ax2.grid(True)
plt.xlabel("Avg Area of Element")
plt.ylabel("Error")

plt.show()
