#include "PME2D.hpp"
#include <iostream>

void PME2D::SetFuncs( double(*u0)(double, double), double (*dir)(double, double, double), double m, double timestep, int M )
{
    mPMEConst = m;
    mTimeStep = timestep;
    mNumTimeSteps = M;
    
    mDirFunc = dir;
    mu0Func = u0;
    
    blaze::DynamicMatrix<double> u(mNumNodes, mNumTimeSteps+1, 0);
    mU = u;
    
    // Set coefficients corresponding to dirichlet conditions
    for (int j = 0; j < mNumNodes; j++)
    {
        if (mDirBoundary[j])
        {
            for (int m=0; m<=mNumTimeSteps; m++)
            {
                mU(j,m) = mDirFunc(mXCoords[j], mYCoords[j], m * mTimeStep);
            }
        }
    }
}

void PME2D::Compute()
{
    // u vector to write over on each iteration
    blaze::DynamicVector<double> u(mNumIntNodes, 0);
    // mass matrix
    blaze::CompressedMatrix<double> mass(mNumIntNodes, mNumIntNodes);

    // vectors to hold quadrature data
    blaze::DynamicVector<double> GaussCoords;
    blaze::DynamicVector<double> GaussCoeffs = mTri.GetQuintGaussCoeffs();
    
    // Iterate over all elements e
    for (int e = 0; e<mNumElements; e++)
    {
        GaussCoords = mTri.GetQuintGaussCoords(e);

        for (int J = 0; J < 3; J++)
        {
            // Gives the number of node which is Jth vertex of element e
            int j = mConArray(e, J) - 1;
            int indice_j = mSecondIndice[mConArray(e, J) - 1];
            
            if (not(mDirBoundary[j]))
            {
                for (int k=0; k<7; k++)
                {
                    u[indice_j] += GaussCoeffs[k] * mu0Func( GaussCoords[2*k], GaussCoords[2*k+1] )
                    * mTri.GetArea(e) * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J );
                }
                
                for (int I = 0; I < 3; I++)
                {
                    // Gives the number of node which is the Ith vertex of element e
                    int i = mConArray(e, I) - 1;
                    int indice_i = mSecondIndice[mConArray(e, I) - 1];
                    
                    if ( not(mDirBoundary[i]) )
                    {
                        // From Jimack document: mass_entry += Area / 6 if I = J
                        //                                  += Area / 12 if I != J
                        if ( I == J )
                        {
                            mass( indice_j, indice_i ) += mTri.GetArea(e) / 6.0;
                        } else
                        {
                            mass( indice_j, indice_i ) += mTri.GetArea(e) / 12.0;
                        }
                    }
                }
            }
        }
    }
    
    blaze::DynamicMatrix<double> LHSmat = mass;
    
    if ( isSymmetric(LHSmat) )
    {
        // If the matrix is symmetric then call cholesky based solver
        posv( LHSmat, u, 'U' );
    } else 
    {
        // if not symmetric then call LU based solver
        const std::unique_ptr<int[]> ipiv( new int[mNumIntNodes] );
        gesv( LHSmat, u, ipiv.get() );
    }
    
    // Place u into mU matrix
    int i=0;
    for (int j=0; j<mNumNodes; j++)
    {
        if (not(mDirBoundary[j]))
        {
            mU(j, 0) = u[i];
            i++;
        }
    }
    
	// Iterate over all time steps
    for (int m=1; m<=mNumTimeSteps; m++)
    {
        blaze::CompressedMatrix<double> K( mNumIntNodes, mNumIntNodes );
        blaze::CompressedMatrix<double> RHSmat( mNumIntNodes, mNumIntNodes );
        
		// Iterate over all elements
        for (int e=0; e<mNumElements; e++)
        {
			GaussCoords = mTri.GetQuintGaussCoords(e);

			// Integrate over three nodes 0, 1, 2
            for (int J=0; J<3; J++)
            {
                // Gives the global number of node which is the Jth vertex of element e
                int j = mConArray( e, J ) - 1;
                int indice_j = mSecondIndice[mConArray(e, J) - 1];
                
                if (not(mDirBoundary[j]))
                {
                    for (int I=0; I<3; I++)
                    {
                        // Gives the number of node which is the Ith vertex of element e
                        int i = mConArray(e, I) - 1;
                        int indice_i = mSecondIndice[mConArray(e, I) - 1];
                        
                        // delta = del(phi_i) . del(phi_j)
                        double delta  = mTri.dxPhiFunc(e, I) * mTri.dxPhiFunc(e, J);
                        delta += mTri.dyPhiFunc(e, I) * mTri.dyPhiFunc(e, J);
                        
                        // RHS matrix is:
                        //     integral over omega ( u^m * delta )
                        double femsol=0;
                        for (int N=0; N<3; N++)
                        {
                            int indice_n = mConArray(e, N) - 1;
                            for (int k=0; k<7; k++)
                            {
                                femsol += mU( indice_n, m-1 ) * mTri.phiFunc(GaussCoords[2*k], GaussCoords[2*k+1], e, N);
                            }
                        }
                    
                        if ( not(mDirBoundary[i]) )
                        {
                            double stiff_entry = 0;
                            double mass_entry = 0;
                            
                            for (int k=0; k<7; k++)
                            {
                                stiff_entry += GaussCoeffs[k] * mTri.GetArea(e) * delta
                                                         * pow( femsol, mPMEConst );
                                mass_entry += GaussCoeffs[k] * mTri.GetArea(e)
                                * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, I )
                                * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, J );
                            }
                            
                            K(indice_j, indice_i) += - mTimeStep * stiff_entry + mass_entry;
                        }
                    }
                }
            }
        }
        
        // Update u with new u
        u = K * u;
        
        // update LHSmat since it is overwritten by gesv
        LHSmat = mass;
        
        if ( isSymmetric(LHSmat) )
        {
            // If the matrix is symmetric then call cholesky based solver
            posv( LHSmat, u, 'U' );
        } else 
        {
            // if not symmetric then call LU based solver
            const std::unique_ptr<int[]> ipiv( new int[mNumIntNodes] );
            gesv( LHSmat, u, ipiv.get() );
        }
        
        // Place u at current time step into mU matrix
        int i=0;
        for (int j=0; j<mNumNodes; j++)
        {
            if (not(mDirBoundary[j]))
            {
                mU(j, m) = u[i];
                i++;
            }
        }
    }
}

double PME2D::FEMSolution( const double x, const double y, const double t )
{
    double sum=0;
    
    int indice_time = (int)(t/mTimeStep);
    
    // Iterate over all elements e
    for (int e = 0; e<mNumElements; e++)
    {
        // First check if the point is in a triangle
        if ( mTri.IsInTri( x, y, e ) )
        {
            for (int J=0; J<3; J++)
            {
                int indice_j = mConArray(e, J) - 1;
                
                // Then add to the sum the output of each basis function
                sum += mU( indice_j, indice_time ) * mTri.phiFunc(x, y, e, J);
            }
            
            // Break if already appeared in one triangle
            break;
        }
    }
    
    return sum;
}

double PME2D::FEMError( double(*exact)( double, double, double ), double p, double t)
{
    double sum = 0;
    int indice_time = (int)(t/mTimeStep);

    // Create vectors to hold gauss quadrature information
    blaze::DynamicVector<double> GaussCoords;
    blaze::DynamicVector<double> GaussCoeffs = mTri.GetQuintGaussCoeffs();
    
    for (int e=0; e<mNumElements; e++)
    {
        // Get the coordinate data for current element
        GaussCoords = mTri.GetQuintGaussCoords(e);
        
        for (int k=0; k<7; k++)
        {
            // Find the FEM solution on current element
            double femsol=0;
            for (int I=0; I<3; I++)
            {
                int indice_i = mConArray( e, I ) - 1;
                femsol += mU(indice_i,indice_time) * mTri.phiFunc( GaussCoords[2*k], GaussCoords[2*k+1], e, I );
            }

            // Find the error
            sum += GaussCoeffs[k] * mTri.GetArea(e) 
                * pow ( fabs ( exact( GaussCoords[2*k], GaussCoords[2*k+1], t ) - femsol ) , p );
        }
    }
    
    return pow(sum, 1 / p);
}

void PME2D::PrintFile( const std::string file_name )
{
    std::ofstream print_file;
    print_file.open( file_name );
    
    print_file << mNumNodes << " " << mNumElements << std::endl;
    for (int i=0; i<mNumNodes; i++)
    {
        print_file << mXCoords[i] << " " << mYCoords[i] << " ";
        for (int t=0; t<=mNumTimeSteps; t++)
        {
            print_file << mU(i, t) << " ";
        }
        print_file << std::endl;
    }
    for (int i=0; i<mNumElements; i++)
    {
        print_file << mConArray(i, 0) << " "
        << mConArray(i, 1) << " "
        << mConArray(i, 2) << " " << std::endl;
    }
    
    print_file.close();
}
