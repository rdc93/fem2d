#ifndef TRIANGLEHEADERDEF
#define TRIANGLEHEADERDEF

#include <blaze/Math.h>

// Class for the triangulation of omega

class Triangle
{
public:
	// Specialised constructor, put each coordinate in component by component
    Triangle( const blaze::DynamicMatrix<int> ConArray, const blaze::DynamicVector<double> x,
              const blaze::DynamicVector<double> y );
    
    Triangle();

	// Calculates the jth linear finite element basis function,
	// for node j, on element e, at point (x,y)
	double phiFunc(const double x, const double y, const int e, const int j );

	// Calculates the derivative wrt x of linear basis func
	// for node j
	double dxPhiFunc( const int e, const int j );

	// Calculates the derivative wrt y of linear basis func
	// for node j
	double dyPhiFunc( const int e, const int j );

	// Returns the area of the triangle
	double GetArea( const int e );

	// Returns true if the point (x,y) is in element e
	bool IsInTri(const double x, const double y, const int e );
    
    // Returns coordinates of the points to use in Gaussian integration
    blaze::DynamicVector<double> GetQuintGaussCoords( const int e );
    
    blaze::DynamicVector<double> GetQuintGaussCoeffs();

private:
	// private matrices to hold the coefficients A, B, C and points X, Y
	// for each node of triangle
	blaze::DynamicMatrix<double> mA;
	blaze::DynamicMatrix<double> mB;
	blaze::DynamicMatrix<double> mC;

    blaze::DynamicVector<double> mArea;
    
    blaze::DynamicMatrix<double> mGaussPoints;
};

#endif
